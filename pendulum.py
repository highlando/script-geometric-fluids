import numpy as np
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from matplotlib import colors as mcolors
#import matplotlib.animation as animation

def pendrhs(pq):
    ''' The right hand side of the pendulum equations `x'=f(x)` '''
    p = pq[0]
    q = pq[1]
    return np.array([-np.sin(q), p])

def pendres(t, x, xdot):
    ''' The pendulum eqns in the form `F(t, x, x')=0` '''
    return xdot - pendrhs(x)

def eulerschemes(F=None, inival=None, interval=[0, 1], Nts=100,
                 scheme='imp', dFdx=None, dFdxdot=None):
    """
    Parameters:
    ---
    F: callable
        the problem in the form F(t,x,x')
    Nts: integer
        Number of time steps
    """
    t0, te = interval[0], interval[1]
    h = 1./Nts*(te-t0)
    N = inival.size

    sollist = [inival.reshape((1, N))]
    tlist = [t0]

    xk = inival

    if scheme == 'imp':
        plotopt = '.'

        def euler_increment(xkkn):
            """ the implicit Euler update for a general F """
            return F(tkk, xkkn, 1. / h * (xkkn - xk)).flatten()

    elif scheme == 'exp':
        plotopt = '+'

        def euler_increment(xkkn):
            """ the explicit Euler update for a general F """
            return F(tkk, xk, 1. / h * (xkkn - xk)).flatten()

    elif scheme == 'midp':
        plotopt = ''

        def euler_increment(xkkn):
            """ the midpt update for a general F """
            return F(tkk, .5 * (xkkn + xk), 1. / h * (xkkn - xk)).flatten()

    elif scheme == 'symp':
        plotopt = ''
        d = xk.size
        if not np.mod(d, 2) == 0:
            raise UserWarning('dimension is not even')
        dbt = np.int(d / 2)

        def euler_increment(xkkn):
            """ the symplectic Euler update for a general F """
            qk = xk[-dbt:]
            pkkn = xkkn[:dbt]
            pkknqk = np.array([pkkn, qk]).reshape(d, )
            return F(tkk, pkknqk, 1. / h * (xkkn - xk)).flatten()

    # perform time integration
    for k in range(1, Nts+1):  # python starts counting with zero...

        tkk = t0 + k*h

        # call a nonlinear solver for the system f(x)=0
        xkk, _, flag, msg = fsolve(func=euler_increment, x0=xk,
                                   full_output=True)
        if not flag == 1:
            print('Caution at t={0}: '.format(tkk) + msg)
        sollist.append(xkk.reshape((1, N)))
        tlist.append(tkk)
        xk = xkk

    sol = np.vstack(sollist)
    return sol, tlist

interval = [0, 50]
inival = np.array([1., 0.])
nSteps = 1000
probdict = dict(F=pendres, inival=inival, interval=interval, Nts=nSteps)
# Caution: sol[:,0] is momentum; sol[:,1] is position
sol_imp, tlist = eulerschemes(scheme='imp', **probdict)
sol_exp, tlist = eulerschemes(scheme='exp', **probdict)
sol_symp, tlist = eulerschemes(scheme='symp', **probdict)

## 
# plotting
colors = dict( mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)

# space time plot

ref = np.ones( sol_imp.shape[0])
plt.figure(1)
plt.plot( sol_imp[:,1])
plt.plot( sol_exp[:,1])
plt.plot( sol_symp[:,1])
plt.plot( ref, '--', color=colors['grey'])
plt.xlabel('t'), plt.ylabel('x')
plt.legend( ['Implicit Euler', 'Explicit Euler', 'Symplectic Euler'],
            loc='upper left', bbox_to_anchor=(1.0, 1.0, 1.0, 0.))

# phase portrait

plt.figure(2)
plt.plot( sol_imp[:,1], sol_imp[:,0])
plt.plot( sol_exp[:,1], sol_exp[:,0])
plt.plot( sol_symp[:,1], sol_symp[:,0])
plt.xlabel('x'), plt.ylabel('p')
plt.title( 'phase portrait')
plt.legend( ['Implicit Euler', 'Explicit Euler', 'Symplectic Euler'],
            loc='upper left', bbox_to_anchor=(1.0, 1.0, 1.0, 0.))

# energy

# the 1.0 = np.cos(0) is to account for that kinetic potential energy is 
# maximal at the initial state and 0 at 
en_imp = 0.5 * sol_imp[:,0]**2 - np.cos( sol_imp[:,1])
en_exp = 0.5 * sol_imp[:,0]**2 - np.cos( sol_exp[:,1])
en_symp = 0.5 * sol_symp[:,0]**2 - np.cos( sol_symp[:,1])

plt.figure(3)
plt.plot( en_imp)
plt.plot( en_exp)
plt.plot( en_symp)
plt.legend( ['Implicit Euler', 'Explicit Euler', 'Symplectic Euler'],
            loc='upper left', bbox_to_anchor=(1.0, 1.0, 1.0, 0.))
plt.title( 'energy')

# animation

#fig3 = plt.figure(3)
#ims = []
#for i in range( 1, nSteps) :
#    ims.append( (plt.plot( sol_symp[:i,0], sol_symp[:i,1]),) )
#im_ani = animation.ArtistAnimation( fig3, ims, interval=200, repeat_delay=3000, blit=True)

plt.show()



